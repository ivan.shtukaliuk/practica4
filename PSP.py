def is_cbs(string):
    a = 0 
    for i in string:
        if a < 0:
            print('Строка не является ПСП\n')
            return None
        elif i == ')' and a != 0:
            a -= 1
        elif i == '(':
            a += 1
    if a != 0:
        print('Строка не является ПСП\n')
    else:
        print('Строка является ПСП\n')
        
        
def need_to_move(string):
    a = 0
    c = 0
    for i in string:
        if i == ')':
            a -= 1
            if a < 0:
                a = 0
                print(f'Скобку под индексом {c} нужно переместить в конец строки\n')
        elif i == '(':
            a += 1
        c += 1
        
        
def menu():
    string = input('Введите строку из круглых скобочек\n')

    while True:
        for k in string:
            if k not in [')', '(']:
                string = input('Неверный ввод строки\n')
                continue
        break
                
    while (i := input('1 - проверить, является ли строка ПСП \n'
                      '2 - найти нужные изменения, чтобы получить ПСП \n'
                      '"exit" - закончить работу программы \n')) != 'exit':
        while i not in ['1', '2']:
            i = input('Незнакомая команда\n')
            if i == 'exit':
                return i
        if i == '1':
            is_cbs(string)
        if i == '2':
            if len(string) % 2 != 0:
                print('Количетсво скобок должно быть четным')
                return 'exit'
            if string.count('(') != string.count(')'):
                print('Количество "(" и ")" должно быть равно')
                return 'exit'
            need_to_move(string)
    return i     
    
    
if __name__ == '__main__':
    while (leave := menu()) != 'exit':
        continue    
